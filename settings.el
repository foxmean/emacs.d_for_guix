(setq user-full-name "Pathompong Kwangtong")
(setq user-mail-address "foxmean@protonmail.com")

(require 'package)
(setq package-archives nil)
(setq package-enable-at-startup nil)
(package-initialize)

(add-to-list 'load-path "~/.emacs.d/lisp/")

(require 'pdf-tools)
(pdf-tools-install)

(require 'ledger-mode)
(setq ledger-clear-whole-transactions 1)
(add-to-list 'auto-mode-alist '("\\.dat\\'" . ledger-mode))

(require 'org)
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
(setq org-confirm-babel-evaluate nil) ; Stop emacs asking for confirmation evaluate code in org-mode
(setq org-src-fontify-natively t) ; Make org mode syntax color embeded source code
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c r") 'org-capture)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c L") 'org-insert-link-global)
(global-set-key (kbd "C-c O") 'org-open-at-point-global)
(global-set-key (kbd "<f9> <f9>") 'org-agenda-list)
(global-set-key (kbd "<f9> <f8>") (lambda () (interactive) (org-capture nil "r")))
(setq org-capture-templates '(
  ("n" "Note" entry (file+headline "~/journals/organizer.org" "Note")
    "**  %? :note:\n   <%<%Y-%m-%d %a>>\n"
    :empty-lines 1)
  ("t" "Task, scheduled (not yet categorize)" entry (file+headline "~/journals/organizer.org" "Task(s): not yet categorized")
    "** TODO [#A] %?\n   DEADLINE: <%<%Y-%m-%d %a>>\n"
    :empty-lines 1)))

(find-file "~/journals/organizer.org")
(org-agenda nil "a")

(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'magit-dispatch)
(global-magit-file-mode)

(require 'diff-hl)
(global-diff-hl-mode)
(add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)

(require 'which-key)
(which-key-mode)

(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)

(global-anzu-mode +1)

(require 'yasnippet)
(yas-global-mode 1)

(global-flycheck-mode)
(add-hook 'haskell-mode-hook #'flycheck-haskell-setup)
(with-eval-after-load 'rust-mode
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

(add-hook 'after-init-hook 'global-company-mode)

(display-time-mode 1)
(setq frame-title-format (concat "Foxmean's Emacs"))
(set-frame-font "Hack-10")
(global-prettify-symbols-mode 1) ; Display “lambda” as “λ”
(global-set-key (kbd "C-x C-l") 'display-line-numbers-mode)
(menu-bar-mode 1)
(tool-bar-mode 0)
(scroll-bar-mode 0)
(setq inhibit-startup-screen t)
(global-subword-mode 1) ; Move by SubWord
(show-paren-mode 1) ; Turn on bracket match highlight
(save-place-mode 1) ; Remember cursor position
(defalias 'yes-or-no-p 'y-or-n-p)
(require 'hide-mode-line) ; For hide mode-line when nedd
(require 'anzu)
(setq anzu-cons-mode-line-p nil)
(global-anzu-mode +1)
