;;; init.el --- Summary: Foxmean's init.el file for Guix
;;; Commentary:
;; I put these Emacs Lisp documentation strings in here (and below) because I really,
;; really, want Flycheck to like me.  I crave its approval.
;;; Code:

;;; Customize-variables
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(custom-enabled-themes (quote (zenburn)))
 '(custom-safe-themes
   (quote
    ("99bc178c2856c32505c514ac04bf25022eaa02e2fddc5e7cdb40271bc708de39" "5f27195e3f4b85ac50c1e2fac080f0dd6535440891c54fcfa62cdcefedf56b1b" "1436d643b98844555d56c59c74004eb158dc85fc55d2e7205f8d9b8c860e177f" "a22f40b63f9bc0a69ebc8ba4fbc6b452a4e3f84b80590ba0a92b4ff599e53ad0" default)))
 '(ledger-reports
   (quote
    (("ledger -f /home/meanix/journals/ledger.dat reg assets\\:" "ledger -f /home/meanix/journals/ledger.dat reg assets\\:")
     ("bal" "%(binary) -f %(ledger-file) bal")
     ("reg" "%(binary) -f %(ledger-file) reg")
     ("payee" "%(binary) -f %(ledger-file) reg @%(payee)")
     ("account" "%(binary) -f %(ledger-file) reg %(account)"))))
 '(org-agenda-files (quote ("~/journals/organizer.org")))
 '(org-todo-keywords
   (quote
    ((sequence "TODO(t)" "WAITING(w)" "SOMEDAY(s)" "DONE(d)"))))
 '(yas-snippet-dirs
   (quote
    ("/home/meanguix/.emacs.d/snippets" "~/.guix-profile/share/emacs/yasnippet-snippets/"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'org)
(org-babel-load-file (expand-file-name "~/.emacs.d/settings.org"))

;;; init.el ends here
